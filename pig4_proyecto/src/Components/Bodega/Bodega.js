import React, { Component, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';


let data = [{ Nombre: "Bodega 1", Direccion: "Av bolivar 11-70 Guatemala", Estado: 'A', CodigoUsuario: 1, CodigoSede: 2 }];
let encargados = [{ id: 1, Nombre: "Juan" }, { id: 2, Nombre: "Luis" }];
let sedes= [{ id: 1, Nombre: "Sede 1" }, { id: 2, Nombre: "Sede 2" }];
let linkbackend = "link";

export default class Bodega extends React.Component {


    //getBodegas();
    state = {
        data: data,
        encargados: encargados,
        sedes: sedes,
        form: {
            Nombre: '',
            Direccion: '',
            Estado: '',
            CodigoUsuario: '',
            CodigoSede: ''
        },
        modalInsertar: false,
        modalEditar: false,
        dropdownOpen: false

    }

    getBodegas = () => {


    }
    getEncargados = () => {


    }

    getSedes = () => {


    }





    insertar = () => {
        var valorNuevo = { ...this.state.form };
        var lista = this.state.data;
        lista.push(valorNuevo);
        //inserto en la bd
        //si se inserto correctamente en la bd  y actualizo el estado
        console.log(valorNuevo);
        this.setState({ modalInsertar: false, data: lista });
    }


    editar = (Bodega) => {
        // si se hizo el update Bodega actualizo el estado 
        var lista = this.state.data;
        this.setState({ modalInsertar: false, data: lista });
    }

    eliminar = (Bodega) => {
        var opcion = window.confirm("Desea eliminar la Bodega?");
        if (opcion) {
            //eliminar
        }
    }



    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    showModalEditar = (registro) => {
        this.setState({ modalEditar: true, form: registro });
    }

    hideModalEditar = () => {
        this.setState({ modalEditar: false });
    }



    getEstado = (estado) => {
       if(estado=='A'){
        return "Activo";
       }else{
        return "Inactivo";
       }
    }

    render() {

        return (

            <div>
                <Container>

                    <nav aria-label="breadcrumb">

                        <Button color="success" onClick={() => this.showModalInsertar()}>
                            Agregar nueva Bodega 2
                     </Button>

                    </nav>
                    <br />

                    <Table dark>
                        <thead>

                        </thead>

                        <th>  Nombre Bodega 2 </th>
                        <th>  Direccion</th>
                        <th>  Estado</th>
                        <th>  opciones</th>


                        <tbody>
                            {this.state.data.map((Bodega) => (
                                <tr>

                                    <td> {Bodega.Nombre}</td>
                                    <td> {Bodega.Direccion}</td>
                                    <td> {this.getEstado(Bodega.Estado)}</td>
                                    <td><Button color="primary" onClick={() => this.showModalEditar(Bodega)}>Editar </Button>
                                        {" "}
                                        <Button color="danger" onClick={() => this.eliminar()}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Agregar Bodega</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                Nombre Bodega:
                             </label>
                            <input
                                className="form-control"
                                name="Nombre"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Direccion:
                            </label>
                            <input
                                className="form-control"
                                name="Direccion"
                                type="text"
                                onChange={this.handleChange}
                            />

                        </FormGroup>
                        <FormGroup controlId="exampleForm.SelectCustom">
                            <label>
                                Estado:
                            </label>
                            <select className="form-control" >
                                <option value='A'>Activo</option>
                                <option value='I' >Inactivo</option>

                            </select>
                        </FormGroup>


                        <FormGroup>
                            <label>
                                Encargado:
                            </label>
                            <select className="form-control" >
                                {this.state.encargados.map((encargado) => (
                                    <option value={encargado.id}>{encargado.Nombre}</option>

                                ))}

                            </select>



                        </FormGroup>
                        <FormGroup>
                            <label>
                                Sede:
                            </label>
                            <select className="form-control" >
                                {this.state.sedes.map((sede) => (
                                    <option value={sede.id}>{sede.Nombre}</option>

                                ))}

                            </select>

                        </FormGroup>


                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Insertar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div><h3>Editar Bodega</h3></div>
                    </ModalHeader>

                   
                    <ModalBody>

                        <FormGroup>
                            <label>
                                Nombre Bodega:
                             </label>
                            <input
                                className="form-control"
                                name="Nombre"
                                type="text"
                                value={this.state.form.Nombre}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Direccion:
                            </label>
                            <input
                                className="form-control"
                                name="Direccion"
                                type="text"
                                value={this.state.form.Direccion}
                                onChange={this.handleChange}
                            />

                        </FormGroup>
                        <FormGroup controlId="exampleForm.SelectCustom">
                            <label>
                                Estado:
                            </label>
                            <select className="form-control" >
                                <option value='A'>Activo</option>
                                <option value='I' >Inactivo</option>

                            </select>
                        </FormGroup>


                        <FormGroup>
                            <label>
                                Encargado:
                            </label>
                            <select className="form-control" >
                                {this.state.encargados.map((encargado) => (
                                    <option value={encargado.id}>{encargado.Nombre}</option>

                                ))}

                            </select>



                        </FormGroup>
                        <FormGroup>
                            <label>
                                Sede:
                            </label>
                            <select className="form-control" >
                                {this.state.sedes.map((sede) => (
                                    <option value={sede.id}>{sede.Nombre}</option>

                                ))}

                            </select>

                        </FormGroup>


                    </ModalBody>


                    <ModalFooter>
                        <Button color="primary" onClick={() => this.editar()}>
                            Editar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalEditar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

