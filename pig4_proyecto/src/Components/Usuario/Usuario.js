import React, { Component, useSate } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';

let data = [{ dpi: "3434", nombre: "juan", fechanac: "Fecha", correo: "asdfas@gmail.com", roles: "si", permisos: "Tambien" }];
let linkbackend="link";
export default class Usuario extends React.Component {
    state = {
        data: data,
        form: {
            dpi: '',
            nombre: '',
            fechanac: '',
            correo: '',
            roles: '',
            permisos: '',
        },
        modalInsertar: false,
        modalEditar: false
    }

    getusuarios=()=>{

        
    }
    
    getroles=()=>{

    }

    
    getpermisos=()=>{

    }


    insertar= ()=>{
        var valorNuevo= {...this.state.form};
        var lista= this.state.data;
        lista.push(valorNuevo);
        //inserto en la bd
        //si se inserto correctamente en la bd  y actualizo el estado
        console.log(valorNuevo);
        this.setState({ modalInsertar: false, data: lista });
      }

      
    editar= (usuario)=>{
      
      // si se hizo el update usuario actualizo el estado 
      var lista= this.state.data;
      this.setState({ modalInsertar: false, data: lista });
    }

    eliminar= (usuario)=>{
        var opcion=window.confirm("Desea eliminar el usuario?");
        if(opcion){
            //eliminar
        }
      }

      

    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    showModalEditar = (registro) => {
        this.setState({ modalEditar: true,form:registro});
    }

    hideModalEditar = () => {
        this.setState({ modalEditar: false });
    }
    render() {

        return (

            <div>
                <Container>
                <nav aria-label="breadcrumb">

                        <Button color="success"  onClick={() => this.showModalInsertar()}>
                            Agregar nuevo usuario
                     </Button>
               
                    </nav>
                    <br />
                
                    <Table dark>
                        <thead>

                        </thead>
                        <th>  DPI</th>
                        <th>  Nombre </th>
                        <th>  FechaNac</th>
                        <th>  Correo</th>
                        <th>Roles </th>
                        <th> Permisos</th>
                        <th> Opciones </th>
                        <tbody>
                            {this.state.data.map((usuario) => (
                                <tr>
                                    <td> {usuario.dpi}  </td>
                                    <td> {usuario.nombre}</td>
                                    <td> {usuario.fechanac}</td>
                                    <td> {usuario.correo}</td>
                                    <td> {usuario.roles}</td>
                                    <td> {usuario.permisos}</td>
                                    <td><Button color="primary" onClick={() => this.showModalEditar(usuario)}>Editar </Button>
                                        {" "}
                                        <Button color="danger" onClick={() => this.eliminar()}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Insertar Usuario</h3></div>
                    </ModalHeader>

                    <ModalBody>
                        <FormGroup>
                            <label>
                                DPI:
                            </label>

                            <input
                                className="form-control"
                                name="dpi"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Nombre:
                             </label>
                            <input
                                className="form-control"
                                name="nombre"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Fecha Nacimiento:
                            </label>
                            <input
                                className="form-control"
                                name="fechanac"
                                type="date"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Correo:
                             </label>
                            <input
                                className="form-control"
                                name="correo"
                                type="email"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Roles:
                            </label>
                            <input
                                className="form-control"
                                name="roles"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Permisos:
                            </label>
                            <input
                                className="form-control"
                                name="permisos"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Insertar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div><h3>Editar Usuario</h3></div>
                    </ModalHeader>

                    <ModalBody>
                        <FormGroup>
                            <label>
                                DPI:
                            </label>

                            <input
                                className="form-control"
                                name="dpi"
                                type="text"
                                onChange={this.handleChange}
                                value={this.state.form.dpi}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Nombre:
                             </label>
                            <input
                                className="form-control"
                                name="nombre"
                                type="text"
                                onChange={this.handleChange}
                                value={this.state.form.nombre}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Fecha Nacimiento:
                            </label>
                            <input
                                className="form-control"
                                name="fechanac"
                                type="date"
                                onChange={this.handleChange}
                                value={this.state.form.fechanac}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Correo:
                             </label>
                            <input
                                className="form-control"
                                name="correo"
                                type="email"
                                onChange={this.handleChange}
                                value={this.state.form.correo}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Roles:
                            </label>
                            <input
                                className="form-control"
                                name="roles"
                                type="text"
                                onChange={this.handleChange}
                                value={this.state.form.roles}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Permisos:
                            </label>
                            <input
                                className="form-control"
                                name="permisos"
                                type="text"
                                onChange={this.handleChange}
                                value={this.state.form.permisos}
                            />
                        </FormGroup>
                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.editar()}>
                            Editar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalEditar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

