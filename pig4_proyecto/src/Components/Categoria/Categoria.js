import React, { Component, useSate } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';

let data = [{ NombreCategoria: "Electrodomesticos", DescripcionCategoria: "Categoria global  de electrodomesticos" }];
let linkbackend = "link";
export default class Categoria extends React.Component {
    //getCategorias();
    state = {
        data: data,
        form: {
            NombreCategoria: '',
            DescripcionCategoria: '',
        },
        modalInsertar: false,
        modalEditar: false
    }

    getCategorias = () => {


    }




    insertar = () => {
        var valorNuevo = { ...this.state.form };
        var lista = this.state.data;
        lista.push(valorNuevo);
        //inserto en la bd
        //si se inserto correctamente en la bd  y actualizo el estado
        console.log(valorNuevo);
        this.setState({ modalInsertar: false, data: lista });
    }


    editar = (Categoria) => {
        // si se hizo el update Categoria actualizo el estado 
        var lista = this.state.data;
        this.setState({ modalInsertar: false, data: lista });
    }

    eliminar = (Categoria) => {
        var opcion = window.confirm("Desea eliminar la Categoria?");
        if (opcion) {
            //eliminar
        }
    }



    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    showModalEditar = (registro) => {
        this.setState({ modalEditar: true, form: registro });
    }

    hideModalEditar = () => {
        this.setState({ modalEditar: false });
    }
    render() {

        return (

            <div>
                <Container>

                    <nav aria-label="breadcrumb">

                        <Button color="success"  onClick={() => this.showModalInsertar()}>
                            Agregar nueva categoria
                     </Button>
               
                    </nav>
                    <br />
                    <Table dark>
                        <thead>

                        </thead>

                        <th>  Nombre Categoria </th>
                        <th>  Descripcion Categoria</th>
                        <th>  Opciones</th>

                        <tbody>
                            {this.state.data.map((Categoria) => (
                                <tr>

                                    <td> {Categoria.NombreCategoria}</td>
                                    <td> {Categoria.DescripcionCategoria}</td>
                                    <td><Button color="primary" onClick={() => this.showModalEditar(Categoria)}>Editar </Button>
                                        {" "}
                                        <Button color="danger" onClick={() => this.eliminar()}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Agregar Categoria</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                Nombre Categoria:
                             </label>
                            <input
                                className="form-control"
                                name="NombreCategoria"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Descripcin Categoria:
                            </label>
                            <input
                                className="form-control"
                                name="DescripcionCategoria"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>


                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Insertar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div><h3>Editar Categoria</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                Nombre Categoria:
                         </label>
                            <input
                                className="form-control"
                                name="NombreCategoria"
                                type="text"
                                value={this.state.form.NombreCategoria}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Descripcin Categoria:
                        </label>
                            <input
                                className="form-control"
                                name="DescripcionCategoria"
                                type="text"
                                value={this.state.form.DescripcionCategoria}
                                onChange={this.handleChange}
                            />
                        </FormGroup>


                    </ModalBody>


                    <ModalFooter>
                        <Button color="primary" onClick={() => this.editar()}>
                            Editar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalEditar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

